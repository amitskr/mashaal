# Mashaal Flashlight App

## Description

Mashaal is a simple and efficient flashlight app for Android devices coded in Java. The app provides a reliable tool for users to quickly turn their device into a flashlight, utilizing the device's camera flash or screen as a light source.

## Features

- **Flashlight Toggle:** Easily turn the flashlight on and off with a simple tap.
- **Screen Light:** In addition to the camera flash, Mashaal also offers the option to use the device's screen as a source of light.
- **Minimalist Design:** The app is designed for simplicity and ease of use, ensuring a straightforward flashlight experience.
- **Battery Efficient:** Mashaal is optimized to minimize battery consumption, allowing users to use the flashlight without worrying about excessive power drain.

## Installation

### F-Droid

Mashaal is available on F-Droid, an open-source app repository. To install Mashaal from F-Droid, follow these steps:

1. Download and install the F-Droid app from [F-Droid's official website](https://f-droid.org/).
2. Open the F-Droid app and search for "Mashaal."
3. Select Mashaal from the search results.
4. Click on the "Install" button to download and install the app on your device.

### Manual Installation

If you prefer to install Mashaal manually, you can download the APK file from the [releases](git@gitlab.com:amitskr/mashaal.git) section of the GitHub repository.

1. Enable installation from unknown sources in your device settings.
2. Download the latest APK from the releases page.
3. Open the downloaded APK to initiate the installation process.
4. Follow the on-screen instructions to complete the installation.

## Permissions

Mashaal requires the following permissions:

- Camera: To access the camera flash and provide the flashlight functionality.
- Wake Lock: To prevent the device from sleeping while the flashlight is in use.

## Contributions

Contributions to Mashaal are welcome! If you encounter any issues or have suggestions for improvement, please open an issue on the [GitHub repository](git@gitlab.com:amitskr/mashaal.git).

## License

Mashaal is licensed under the [GNU General Public License](LICENSE). See the [LICENSE](LICENSE) file for details.

## Contact

For any questions or inquiries, please contact the developer at amit15334@gmail.com.

Enjoy using Mashaal!
